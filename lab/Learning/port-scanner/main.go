package main

import (
	"fmt"
	"net"
)

func main() {
	var i uint
	for i = 1; i < 65535; i++ {
		//addr := fmt.Sprintf("zenspace.lan:%d", i)

		conn, err := net.Dial("tcp", "zenspace.lan:80")

		if err != nil {
			//fmt.Println("Port is closed.")
		//  If the connection worked...
		} else if err == nil {
			fmt.Printf("Port %d is open.\n", i)
			conn.Close()
		}
	}
}
