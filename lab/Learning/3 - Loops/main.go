package main

import (
	"fmt"
	"bufio"
	"os"
	"time"
	"math/rand"
	"strconv"
)

func guess(number int) {
	rand.Seed(time.Now().UnixNano()) // Create random seed
	correctNumber := rand.Intn(10-1) // Generate random number
	fmt.Printf("The correct number is: %v", correctNumber )
	switch number {
		case correctNumber:
			fmt.Println("You got it!")
		
	}
}

func main() {

	// Create a new scanner object from Stdin
	scanner := bufio.NewScanner(os.Stdin)

	// Create infinite with i set to 0, with
	// each iteration increase i by 1.
	for i := 0;; i++ {
		// Take user input
		scanner.Scan()

		userGuess, err := strconv.Atoi(scanner.Text())

		if err != nil {
			panic(err)
		} else {
		guess(userGuess)
		
		// Print the iteration
		fmt.Println(i)	}

	}
}