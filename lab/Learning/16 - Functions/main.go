package main

import "fmt"

// Same function but returns two values
//func add(x, y int) (int, int){
	//return x + y, x - y
//}

// Simple function to return x + y
// func add(x, y int) int{
// 	return x + y
// }

func add(x, y int) (z1, z2 int) {
	// This executes at the end of the function
	//defer fmt.Println("Returning")
	z1 = x + y
	z2 = x - y
	//fmt.Println("Before return")
	return
}

func main() {
	x := 5
	y := 5
	ans1, ans2 := add(x,y)
	fmt.Printf("%d + %d = %v, %d - %d = %v", x, y, ans1, x, y, ans2)
	
}