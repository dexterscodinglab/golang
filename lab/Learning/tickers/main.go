package main

import (
	"bufio"
	"fmt"
	"os"
	"time"
)

func main() {
	ticker := time.NewTicker(500 * time.Millisecond)
	done := make(chan bool)

	go func() {
		for {
			select {
			case <-done:
				return
			case t := <-ticker.C:
				fmt.Println("Tick at", t)
			}
		}
	}()

	//  New scanner object
	scanner := bufio.NewScanner(os.Stdin)
	//  If user presses enter, advance
	scanner.Scan()
	//  Stop ticker
	ticker.Stop()
	done <- true
	fmt.Println("Ticker stopped")
}