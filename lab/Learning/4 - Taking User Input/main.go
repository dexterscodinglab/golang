package main

import (
	"fmt"
	//"bufio"
	//"os"
	//"strconv"
)

func addNum(number, number2 int) (result int){
	result = number + number2
	return 
}

func main() {
	//  Create variables
	var x, y int

	fmt.Print("Enter x >>>: ")
	fmt.Scan(&x)
	fmt.Print("Enter y >>>: ")
	fmt.Scan(&y)
	fmt.Println(addNum(x,y))

}

/*  Basic method to take user input
func main() {
	//  Creates a new scanner object from
	//  stdin
	scanner := bufio.NewScanner(os.Stdin)
	//  Split the words into individual words,
	//  that is if the user types "test test",
	// split it into "test" and "test".
	scanner.Split(bufio.ScanWords)
	//  For each line in the scanner
	for scanner.Scan() {
		//  Output the input
		fmt.Println(scanner.Text())

	}
} */