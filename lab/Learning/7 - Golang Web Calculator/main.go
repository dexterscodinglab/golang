package main

import (
	"fmt"
	"html/template"
	"math"
	"net/http"
	"strconv"
)

//  Fields that will be imported by the calculator.
type ContactDetails struct {
	float1		string
	float2		string
	Operation 	string
}

func main() {
	//  Import the forms.html template file.
	tmpl := template.Must(template.ParseFiles("forms.html"))

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request){
		//  If the request method is not POST...
		if r.Method != http.MethodPost {
			//  ...Write nil as response(?)
			tmpl.Execute(w, nil)
			return
		}

		//  Import the details from the forms.html
		var details = ContactDetails{
			float1:		r.FormValue("input1"),
			float2:		r.FormValue("input2"),
			Operation:	r.FormValue("ope"),
		} //  Do something with details

		//  Convert the input floats to Float64 data type
		float1, _ := strconv.ParseFloat(details.float1, 64)
		float2, _ := strconv.ParseFloat(details.float2, 64)

		//  Create the result variable
		var res float64

		//  Manipulate floats depending on operator
		if details.Operation == "Add" {
			//  I.e add them together
			res = float1 + float2
		} else if details.Operation == "Subtract" {
			res = float1 - float2
		} else if details.Operation == "Divide" {
			res = float1 / float2
		} else if details.Operation == "Multiply" {
			res = float1 * float2
		}

		//  Round the result up
		res = math.Round(res*100) / 100

		_ = details

		//  Serve result
		tmpl.Execute(w, struct{ Res float64}{res})
	})
	fmt.Println("Serving on port 8080")
	http.ListenAndServe(":8080", nil)
}