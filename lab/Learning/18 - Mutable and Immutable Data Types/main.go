package main

import "fmt"

func main() {
	//  Slice
	//var x[]int = []int{3,4,5}
	//  Array
	var x [2]int = [2]int{3,4}
	
	//  Another name for x
	y := x
	
	//  Modifies x too
	y[0] = 100
	
	fmt.Println(x, y) //  x = [3,4] but y = [100, 4]

	//  This is because it creates a new copy of x in y, and
	//  changing y does not actually change x.
}