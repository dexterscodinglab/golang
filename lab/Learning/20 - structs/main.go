package main

import (
	"fmt"
)

type Point struct {
	x int32
	y int32
	//isOnGrid bool
}

func main() {
	//var p1 Point = Point{1,2,true}

	//  Sets x to 1 and y to 2.
	p1 := Point{1, 2}
	//  Sets x but not y (thus y is 0)
	p2 := Point{x: 3}

	//  Prints 1 (the value of x)
	fmt.Println(p1.x, p2.y)
	
}