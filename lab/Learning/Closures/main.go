package main

import "fmt"

//  Counter function
func intSeq() func() int {
	i := 0
	return func() int {
		i++
		return i
	}
}

func main() {
	nextInt := intSeq()

	fmt.Println(nextInt())
	fmt.Println(nextInt())
	fmt.Println(nextInt())
	fmt.Println(nextInt())

	newInts := intSeq()    //  Creates a new unique counter
	fmt.Println(newInts()) //  1
	fmt.Println(nextInt()) //  5
}
