package main

import "fmt"

type Point struct {
	x int32
	y int32
}

type Circle struct {
	radius float64
	center *Point
	//  The name is optional:
	//  *Point works too but
	//  then you need to do c1.x
	//  not c1.center.x
}

func changeX(pt *Point) {
	pt.x = 100
}

func main() {
	p1 := &Point{y: 3}
	//  Since circle takes a Point pointer,
	//  we need to put &Point, to reference
	//  the pointer of Point (a bit confusing) 
	c1 := Circle{4.56, &Point{4,5}}

	//  {0, 3}
	fmt.Println(p1)
	changeX(p1)
	//  {100, 3}
	fmt.Println(p1)
	//  4 5
	fmt.Println(c1.center.x, c1.center.y)

	
}