package main

import (
	"fmt"
	"time"
)

func f(from string) {
	for i := 0; i > -1; i++ {
		fmt.Println(from, ":", i)
	}
}

func main() {
	//f("direct")

	//  Invokes as goroutine, executing concurrently with the one above.
	go f("go1")
	go f("go2")
	go f("go3")
	go f("go4")

	go func(msg string) {
		fmt.Println(msg)
	}("going")

	time.Sleep(time.Second)
	fmt.Println("Done")

}
