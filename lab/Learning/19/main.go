package main

import (
	"fmt"
	//"strconv"
)

func changeNum(x, y int, ope string) (result string){
	if ope == "add" {
		result = x + y
	} else if ope == "sub" {
		result = "placeholder"
	} else if ope == "mul" {
		result = "placeholder"
	} else if ope == "div" {
		result = "placeholder"
	} else {
		result = "syntax error: invalid operator"
	}
	return
}

func main() {
	fmt.Println(changeNum(1,2,"add"))
}
