package main

import (
	"bufio"
	"fmt"
	"os"
	"time"
)

//  Channel synchronization
//
//  Worker function
func worker(done chan bool) {
	fmt.Println("working...")
	time.Sleep(time.Second)
	fmt.Println("done!")

	//  Send done notification
	done <- true
}

func main() {
	//  Creates a new string channel
	messages := make(chan string)

	//  Send the value "ping" into the channel
	go func() { messages <- "ping" }()

	//  Receive the value through the channel
	msg := <-messages

	//  Print the value
	fmt.Println(msg)

	//  By default channels are unbuffered meaning they
	//  only accept sends if there is a corresponding
	//  receive (<- chan) ready.
	//
	//  They can also be buffered:
	newMessages := make(chan string, 2)

	newMessages <- "buffered"
	newMessages <- "channel"

	fmt.Println(<-newMessages)
	fmt.Println(<-newMessages)

	done := make(chan bool, 1)
	go worker(done)

	//  Block until the worker gives notification
	<-done

	//  Take user input concurrently
	go func() {
		fmt.Print("Enter text >>>: ")
		//  New scanner object
		scanner := bufio.NewScanner(os.Stdin)
		scanner.Scan()
		//  Send the input to channel
		newMessages <- scanner.Text()
	}()

	select {
	//  If the user has entered input
	case name := <-newMessages:
		//  Print the input
		fmt.Println(name)
	//  If the user hasn't entered input
	//  within 5 seconds
	case <-time.After(5 * time.Second):
		//  Timeout the call
		fmt.Println("Timed out")
	}
}
