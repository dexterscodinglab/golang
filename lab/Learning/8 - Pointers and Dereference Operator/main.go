package main

import (
	"fmt"
)

//  Function that takes a pointer
func changeValue(str *string){
	*str = "changed"
}

func changeValue2(str string){
	str = "changed"
}

func main() {
	 //  pointer: &
	//   dereference: *
	x := 7
	//  y is equal to the location of x
	y := &x  
	fmt.Println(x,y)
	//  Changes x to 8
	/*
	*y = 8
	fmt.Println(x, y)
	toChange := "hello"
	changeValue(&toChange)
	changeValue2(toChange)
	fmt.Println(toChange)
	*/
}