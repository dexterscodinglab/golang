package main

import (
	"fmt"
	"math"
)

func main() {
	var x,y,z = 5.5, 5, "5.55"

	//  Converting a Int to Float64
	var n = x * float64(y)

	//  Round Float to Int (importing math)
	n = math.Round(n)
	
	//  Output x (555.55) is ... (2 decimals)
	fmt.Printf("x (%f) is of type %T\n",x, x)

	//  Output x (555.5500) is ... (4 decimals)
	fmt.Printf("x (%.4f) is of type %T\n",x, x)

	fmt.Printf("y (%d) is of type %T\n",y, y)
	fmt.Printf("z (%s) is of type %T\n",z, z)
	fmt.Printf("n (%f) is the result of %.1f * %d\n",n,x,y)
}