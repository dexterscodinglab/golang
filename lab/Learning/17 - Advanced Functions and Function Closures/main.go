package main

import "fmt"

// func test(x int) {
// 	fmt.Println("Hello", x)
// }

//  Function test2 that takes a function that accepts
//  an integer and returns an integer.
func test2(myFunc func(int) int) {
	// Calls test from test2
	fmt.Println(myFunc(7))
}


	//  test() // Calls the function
	//  test // References the function

	/*  Assigns a function to a variable
	x := test
	//  Calls the function through the variable
	x(5) */

	/*  Storing a function inside a function
	 test := func(x int) {
	 	fmt.Println(x)
	 }
	 test(5) */

	//  Creates a function 
	/*	
	func() {
		fmt.Println("test")
	}() //  Calls the function */

func returnFunc(x string) func() {
	//  Creates a function to print x and
	//  then returns it to returnFunc
	return func() { fmt.Println(x) }
}

func main() {
	x := returnFunc("Hello")
	x()
}

/*
func main() {
	//  Function to calculate x * -1
	test := func(x int) int {
		return x * -1
	} //(8) //  Calls the function right away

	//  Calls test2 using the test function (which
	//  takes an int and returns one).
	test2(test)
}
*/