package main

import (
	"fmt"
)

func main() {
 	// Map with string keys but int values, maybe apples:1
	// Maps does not keep track of order
	var mp map[string]int = map[string]int{
		"apple":5,
		"pear":6,
		"orange":9,
	}

	// fmt.Println(mp["apple"]) // Access the value of "apple"
	// mp["apple"] = 900 // Changes the value of apple
	// mp["grapes"] = 50 // Adding values
	// delete(mp, "apple") // Deletes apple

	// Another method of making a map (an empty map)
	//mp := make(map[string]int)

	// Check if apple exists, and if it does, store the value in val
	// otherwise, make val the default value of the map and set the ok
	// variable to if it exists or not.
	val, ok := mp["apple"]
	//fmt.Println(val, ok) // Output: 5 true

	fmt.Println(mp)
}
