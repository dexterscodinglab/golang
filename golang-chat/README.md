# Golang chat server

Simple websocket chat server taken from:
https://www.youtube.com/watch?v=JuUAEYLkGbM

Connect using web browser inspect tool, then in console:

`let socket = new WebSocket("ws://localhost:1337/ws")` - to connect

`socket.onmessage = (event) => { console.log("recieved from the server: ", event.data) }`  - To show messages

`socket.send("hello from client1")` - Send message